# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys


# Let Sphinx see the code module for automatic API docs generation
sys.path.insert(0, os.path.abspath('..'))


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Section alignment image suggestions'
copyright = 'MMXXIII-present, a Wikimedia Foundation project'
author = 'Cormac Parle, Marco Fossati, Matthias Mullie, Muniza Aslam, Xabriel Collazo'
release = '0.6.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.viewcode',
    'sphinx_autodoc_typehints',
]

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#configuration

autodoc_default_options = {
    'members': True,
    'member-order': 'bysource',  # Sort by source code order, i.e., pipeline steps
}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'

# https://pradyunsg.me/furo/customisation/

html_logo = '_static/logo.jpg'
html_title = 'Section alignment image suggestions'


# -- Extension configuration -------------------------------------------------

intersphinx_mapping = {
    'python': ('https://docs.python.org/3.10/', None),
    'mwparserfromhell': ('https://mwparserfromhell.readthedocs.io/en/v0.6.4/', None),
    'pandas': ('https://pandas.pydata.org/pandas-docs/version/1.5/', None),
    'pyspark': ('https://spark.apache.org/docs/3.1.2/api/python/', None),
    'section_topics': ('https://section-topics.readthedocs.io/en/latest/', None),
}
