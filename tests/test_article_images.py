# -*- coding: utf-8 -*-
from typing import List

import mwparserfromhell as mwp  # type: ignore
import pytest

from imagerec.article_images import SectionImages, _process_sections, get_images


class TestGetImages:
    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                '[[File:A_portrait_of_A,_by_B–C.PNG|thumb|center|x60px|alt=Foo bar baz|Example image]]',
                ['A_portrait_of_A,_by_B–C.PNG'],
            ),
            (
                "[[File:Example - ita, 1825 - 766672 R (cropped).jpeg|thumb|left|The ''[[example]]]'' image.]]",
                ['Example_-_ita,_1825_-_766672_R_(cropped).jpeg'],
            ),
        ],
    )
    def test_image(self, wikitext: str, expected: List[str]) -> None:
        assert get_images(wikitext) == expected

    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                """{{Infobox book
                    | name           = Anne of Green Gables
                    | image          = FirstPageGreenGables.gif
                    | caption        = The front (first) page of the first edition
                    | website        = https://www.anneofgreengables.com/
                    }}""",
                ['FirstPageGreenGables.gif'],
            ),
            (
                """{{Infobox album
                    | name       = Modal Soul
                    | type       = studio
                    | artist     = [[Nujabes]]
                    | cover      = ModalMusic.jpg
                    }}""",
                ['ModalMusic.jpg'],
            ),
        ],
    )
    def test_infobox(self, wikitext: str, expected: List[str]) -> None:
        assert get_images(wikitext) == expected

    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                """<gallery widths="170" heights="170">
                MET 1984 482 237872.jpg|Seal; 3000–1500 BC; baked [[steatite]]; 2 × 2&nbsp;cm; [[Metropolitan Museum of Art]] (New York City)
                File:Stamp seal and modern impression- unicorn and incense burner (?) MET DP23101 (cropped).jpg|Stamp seal and modern impression: unicorn and incense burner (?); 2600-1900&nbsp;BC; burnt steatite; 3.8 × 3.8 × 1&nbsp;cm; Metropolitan Museum of Art
                Clevelandart 1973.160.jpg|Seal with two-horned bull and inscription; 2010 BC; steatite; overall: 3.2 x 3.2&nbsp;cm; [[Cleveland Museum of Art]] ([[Cleveland]], [[Ohio]], US)
                </gallery>""",
                [
                    'MET_1984_482_237872.jpg',
                    'Stamp_seal_and_modern_impression-_unicorn_and_incense_burner_(?)_MET_DP23101_(cropped).jpg',
                    'Clevelandart_1973.160.jpg',
                ],
            ),
            (
                """{{Gallery
                   |title=Example gallery
                   |width=160 | height=170
                   |align=center
                   |footer=Example 1
                   |File:An Image taken between 1950-1960.JPG
                    |An example [[image]] taken in the [[1950s]]
                    |alt1=Example alt text for an example image
                   }}""",
                ['An_Image_taken_between_1950-1960.JPG'],
            ),
        ],
    )
    def test_unnamed_parameter(self, wikitext: str, expected: List[str]) -> None:
        assert get_images(wikitext) == expected

    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                # https://phabricator.wikimedia.org/T335180
                '{{wide image|Schönbrunn, Viedeň, Rakúsko.jpg|800px|[[Schönbrunn Palace]]}}',
                ['Schönbrunn,_Viedeň,_Rakúsko.jpg'],
            ),
            (
                '{{wide image|other param|Schönbrunn, Viedeň, Rakúsko.jpg|800px|[[Schönbrunn Palace]]}}',
                ['Schönbrunn,_Viedeň,_Rakúsko.jpg'],
            ),
        ],
    )
    def test_gallery(self, wikitext: str, expected: List[str]) -> None:
        assert get_images(wikitext) == expected

    @pytest.mark.parametrize(
        'wikitext,expected',
        [
            (
                '[[A_portrait_of_A,_by_B–C.PNG|thumb|center|x60px|alt=Foo bar baz|Example image]]',
                [],
            ),
            (
                "[[  Example - ita, 1825 - 766672 R (cropped).jpeg|thumb|left|The ''[[example]]]'' image.]]",
                [],
            ),
        ],
    )
    def test_invalid_prefix(self, wikitext: str, expected: List[str]) -> None:
        assert get_images(wikitext) == expected


@pytest.mark.parametrize(
    'wikitext,expected',
    [
        # Test content extraction & skipping
        (
            '==heading==',  # Content is `None`: skip
            [],
        ),
        (
            '==heading==\n',  # Content is an empty string: skip
            [],
        ),
        (
            '==heading==\nIM2short',  # Content is too short: skip.
            [],
        ),
        (
            '==heading==\n* Foo\n* Bar',  # Content has a list: skip
            [],
        ),
        (
            '==heading==\nTable below:\n{| Foo || bar |}',  # Content has a table: skip
            [],
        ),
        (
            '==foo==\n\n==bar==\nSomething not to skip\n\n==baz==\n\n# spam\n# spam\n# spam',
            [SectionImages(index=2, heading='bar', images=[])],
        ),
        # Test heading formatting. Remember that the section index starts from 1
        (
            '==HeadinG==\nFoo bar baz\n',  # Test heading normalization
            [SectionImages(index=1, heading='HeadinG', images=[])],
        ),
        (
            '== Heading ==\nFoo bar baz\n',
            [SectionImages(index=1, heading='Heading', images=[])],
        ),
        (
            '==    Heading    ==\nFoo bar baz\n',
            [SectionImages(index=1, heading='Heading', images=[])],
        ),
        (
            '==Heading(1-2).==\nFoo bar baz\n',
            [SectionImages(index=1, heading='Heading(1-2).', images=[])],
        ),
        # Test reference tags
        (
            '==REFZ<ref>very important ref</ref><ref ... /><ref_name ..></ref> ==\nskipping is fine\n',
            [],
        ),
        (
            '==Latvijas_likumdošanā<ref_name=":0"_/>==\n\n\nplz skip me!\n\n\n'
            '==Geschiedenis<ref>Cold_Lake:_About_Cold_Lake{{dode_link|datum=september_2017_|bot=InternetArchiveBot_}}</ref>==\n\nskipping encouraged\n\n'
            '==Rosa<ref_name=rosa>{{Cita_web|url=http://www.storiapiacenza1919.it/rosa64.htm|titolo=Rosa_1964-1965|editore=Storiapiacenza1919.it}}</ref>==\nskip it\n',
            [],
        ),
    ],
)
def test__process_sections(wikitext: str, expected: list) -> None:
    # See prod `minimum_section_size` default at `article_images.MINIMUM_SECTION_CHARS`
    assert _process_sections(wikitext, minimum_section_size=9) == expected
