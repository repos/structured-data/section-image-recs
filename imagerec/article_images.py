#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""This is the first task of the section alignment image suggestions data pipeline:
to extract images available in Wikipedia article sections.

Inputs come from Wikimedia Foundation's `Analytics Data Lake <https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake>`_:

- `Wikipedias wikitext <https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Content/Mediawiki_wikitext_current>`_
  (all Wikipedias by default as per
  `wikipedias.json <https://gitlab.wikimedia.org/repos/structured-data/section-image-recs/-/blob/02316711725ca4bc72674a7327a0f90ae7564f0c/imagerec/data/wikipedias.json>`_
  )
- `Wikidata item page links <https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Content/Wikidata_item_page_link>`_

High-level steps:

- look up Wikidata `QIDs <https://www.wikidata.org/wiki/Wikidata:Glossary#QID>`_ from article page IDs
- gather image file names from sections (lead one excluded)
  via the `MediaWiki parser from hell <https://mwparserfromhell.readthedocs.io>`_
- filter out sections with standard lists and tables, which typically don't convey relevant content

Output :class:`pyspark.sql.DataFrame` row example:

========  =======  =====================  ================================================================  =======
item_id   page_id  page_title             article_images                                                    wiki_db
========  =======  =====================  ================================================================  =======
Q5861764  1248254  Fiesta_del_Gran_Poder  [{1, Descripción, [La_Paz_Bolivia_Tata_Danzante.jpg, ...]}, ...]  eswiki
========  =======  =====================  ================================================================  =======

More `documentation <https://www.mediawiki.org/wiki/Structured_Data_Across_Wikimedia/Section-level_Image_Suggestions/Data_Pipeline#How_it_works>`_
lives in MediaWiki.
"""

import argparse
import logging
import re

from dataclasses import dataclass
from importlib import resources
from typing import List, Optional, Pattern

import mwparserfromhell as mwp  # type: ignore

from pyspark.sql import (  # type: ignore
    DataFrame,
    SparkSession,
    functions as F,
    types as T,
)

from imagerec.utils import load_wp_codes, wp_code_to_wiki_db


# The usual output size with no `coalesce` is roughly 933 files per partition:
# 298k files across 319 directories (read wikis, as we previously partitioned by them).
# Decrease the size by an order of magnitude circa.
COALESCE = 100
IMAGE_NAME_PREFIX = r"""
(?:
      =          # equals sign if in a template as named parameter
    | \|(?!.+=)  # or a pipe, as unnamed (positional) parameter in a template (i.e. not followed by `paramname=`)
    | :          # or a colon if of the form file:foo.jpg
    | \n         # or newline if in a gallery tag
)
"""
ALLOWED_IMAGE_NAME_CHARACTERS = r"""
    # any character excluding carriage return, newline,
    # hash, angle brackets, square brackets, vertical bar,
    # colon, braces and forward slash
    [^\r\n\#\<\>\[\]\|:\{\}/]
"""
IMAGE_RE = re.compile(
    rf"""
    {IMAGE_NAME_PREFIX}                 # prefix
    ({ALLOWED_IMAGE_NAME_CHARACTERS}+)  # filename
    \.(\w+)                             # extension with period
    """,
    re.VERBOSE,
)
IMAGE_EXTENSIONS = [
    'jpg',
    'png',
    'svg',
    'gif',
    'jpeg',
    'tif',
    'bmp',
    'webp',
    'xcf',
]
# Copied from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/a11b6e70f2b00d039b05715167545d6abc284717/section_topics/pipeline.py#L42
# TODO refactor into a shared component,
#      see https://phabricator.wikimedia.org/T330841 and https://phabricator.wikimedia.org/T331522
LIST_OR_TABLE_RE = re.compile(r'^\*|^#|\{\|', re.MULTILINE)
# Copied from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/095f8d75b89998be63e26d543fe187ee0f54a33c/section_topics/pipeline.py#L52
# TODO refactor into a shared component,
#      see https://phabricator.wikimedia.org/T331522
MINIMUM_SECTION_CHARS = 500
# Copied from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/main/section_topics/pipeline.py
# List of known allowable HTML tags, used to strip tags from section titles
# list based on mediawiki/includes/parser/Sanitizer.php's getRecognizedTagData()
RECOGNIZED_HTML_TAGS = [
    'tt',
    'wbr',
    'br',
    'sup',
    'var',
    's',
    'ruby',
    'sub',
    'ul',
    'abbr',
    'hr',
    'p',
    'li',
    'ins',
    'cite',
    'pre',
    'small',
    'bdi',
    'h6',
    'em',
    'h1',
    'rp',
    'table',
    'strike',
    'data',
    'blockquote',
    'i',
    'div',
    'mark',
    'center',
    'dd',
    'tr',
    'h4',
    'u',
    'rt',
    'span',
    'link',
    'td',
    'dt',
    'th',
    'strong',
    'caption',
    'big',
    'rtc',
    'dfn',
    'time',
    'code',
    'dl',
    'del',
    'samp',
    'b',
    'font',
    'rb',
    'h5',
    'bdo',
    'ol',
    'kbd',
    'q',
    'h2',
    'meta',
    'h3',
]


@dataclass
class SectionImages:
    """:func:`dataclasses.dataclass` that stores image file names available
    in a Wikipedia article section.

    :param index: a section numerical index
    :param heading: a section heading
    :param images: a list of image file names
    """

    index: int
    heading: str
    images: List[str]


section_images_schema = T.ArrayType(
    T.StructType(
        [
            T.StructField('index', T.IntegerType(), nullable=False),
            T.StructField('heading', T.StringType(), nullable=False),
            T.StructField('images', T.ArrayType(T.StringType()), nullable=False),
        ]
    )
)


# Adapted from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/a11b6e70f2b00d039b05715167545d6abc284717/section_topics/pipeline.py#L158
# TODO refactor into a shared component,
#      see https://phabricator.wikimedia.org/T330841 and https://phabricator.wikimedia.org/T331522
def _has_list_or_table(
    wikitext: str, list_or_table_re: Pattern = LIST_OR_TABLE_RE
) -> bool:
    # Tell if the given wikitext contains at least one list or table.

    if list_or_table_re.search(wikitext) is not None:
        return True

    return False


# Copied from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/b6734deb533984dd9cd4e8af579b6ecd77da7e23/section_topics/pipeline.py#L357
# TODO refactor into a shared component,
#      see https://phabricator.wikimedia.org/T331522
def _title_contains_ref_tag(title: str) -> bool:
    # Tell if the given wikitext title contains a reference tag

    if re.search(r'<ref.+/>', title) is not None:
        return True

    if (
        re.search(r'<ref[^>]*>.*?</ref>', title, flags=re.DOTALL | re.IGNORECASE)
        is not None
    ):
        return True

    return False


# Copied from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/b974c295eceec51d1c4b98ed84af50a4857da719/section_topics/pipeline.py#L167
# TODO refactor into a shared component,
#      see https://phabricator.wikimedia.org/T331522
def _is_content_longer_or_equal_than(
    wikitext: Optional[str], minimum_section_size: int
) -> bool:
    # Tell if the given wikitext is at least as long as minimum_section_size.
    # If minimum_section_size <= 0, then we do no check, and return True. Useful for testing.
    if minimum_section_size <= 0:
        return True
    if wikitext is not None and len(wikitext) >= minimum_section_size:
        return True

    return False


# Copied from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/main/section_topics/pipeline.py
def wikitext_headings_to_anchors(headings: List[str]) -> List[str]:
    """Same as :func:`section_topics.pipeline.wikitext_headings_to_anchors`."""

    def transform(text: str) -> str:
        # Remove `=` and whitespace surrounding the heading text
        text = re.sub(r'^(={1,6})\s*(.*?)\s*\1$', r'\2', text)
        # Remove HTML tags
        text = re.sub(fr'<\/?({"|".join(RECOGNIZED_HTML_TAGS)})(\s[^>]*)?>', '', text)
        # Remove link wikitext markup
        text = re.sub(r'\[\[(.+?)\]\]', r'\1', text)
        text = re.sub(r'(?<!\[)\[[^\[]*?(?=//).+? (.+?)\](?!\[)', r'\1', text)
        # Remove italics(''), bold(''') or combination (''''') wikitext markup
        text = re.sub("'''''|'''|''", '', text)
        # Spaces & underscores collapse to 1 underscore
        text = re.sub('[ _]+', '_', text)
        # Tabs to underscores; 1 for each tab
        text = re.sub(r'\t', '_', text)

        return text

    anchors = [transform(heading) for heading in headings]

    # Append the `_N` suffix to duplicate anchors
    return [
        a if not a in anchors[0:i] else f'{a}_{anchors[0:i].count(a) + 1}'
        for i, a in enumerate(anchors)
    ]


def get_images(wikitext: str) -> List[str]:
    """Grab all image file names from a wikitext.

    :param wikitext: a wikitext
    :return: the list of image file names
    """
    images = [
        f'{name}.{extension}'.strip().replace(' ', '_')
        for name, extension in IMAGE_RE.findall(wikitext)
        if extension.lower() in IMAGE_EXTENSIONS
    ]
    return images


def _process_sections(
    wikitext: str, minimum_section_size: int = MINIMUM_SECTION_CHARS
) -> list:
    parsed_wikicode = mwp.parse(wikitext, skip_style_tags=True)
    sections = parsed_wikicode.get_sections(
        levels=[2], include_headings=True, include_lead=False
    )
    # Section index starts from 1 because the lead section is excluded
    start_index = 1

    headings = [str(heading) for heading in parsed_wikicode.filter_headings()]
    anchors = wikitext_headings_to_anchors(headings)

    # For each section:
    # - extract heading and content
    # - skip if the content is empty
    # - skip if the content has at least one standard list of table
    # - format the heading

    images = []
    heading_index = 0

    # Adapted from https://gitlab.wikimedia.org/repos/structured-data/section-topics/-/blob/a11b6e70f2b00d039b05715167545d6abc284717/section_topics/pipeline.py#L213
    for index, section in enumerate(sections, start=start_index):
        headings_in_section = section.filter_headings()
        heading = anchors[heading_index]
        heading_index += len(headings_in_section)
        section.remove(headings_in_section[0])
        content = str(section).strip()

        if not content:
            continue

        # TODO add an optional CLI arg that passes `minimum_section_size` here via UDF currying
        if not _is_content_longer_or_equal_than(content, minimum_section_size):
            continue
        # TODO make this optional via UDF currying + passing a `keep_lists_and_tables` boolean
        if _has_list_or_table(content):
            continue
        if _title_contains_ref_tag(heading):
            continue

        images.append(
            SectionImages(index=index, heading=heading, images=get_images(content))
        )

    return images


@F.udf(returnType=section_images_schema)
def extract_section_images(wikitext: str) -> Optional[List[SectionImages]]:
    """Parse a wikitext into section indices, titles, and images.
    The lead section is excluded.

    .. note::

        This is the core function reponsible for the data heavy lifting.
        It's implemented as a PySpark user-defined function (:func:`pyspark.sql.functions.udf`).


    :param wikitext: a wikitext
    :return: the list of :class:`SectionImages`
    """
    try:
        return _process_sections(wikitext)
    except Exception as e:
        logging.exception(f"Couldn't parse article: {wikitext}", e)
        return None


def get_article_images(
    spark: SparkSession,
    wikitext_snapshot: str,
    item_page_link_snapshot: str,
    wiki_dbs: Optional[List[str]],
) -> DataFrame:
    """Extract images available in Wikipedia article sections.

    :param spark: an active Spark session
    :param wikitext_snapshot: a ``YYYY-MM`` date
    :param item_page_link_snapshot: a ``YYYY-MM-DD`` date
    :param wiki_dbs: a list of wikis to process. Pass ``None`` for all wikis
    :return: the dataframe of section images
    """
    articles_df = spark.sql(
        f"""SELECT
                ipl.item_id,
                wt.page_id,
                replace(wt.page_title, ' ', '_') as page_title,
                wt.wiki_db,
                wt.revision_text
            FROM wmf.mediawiki_wikitext_current as wt
            INNER JOIN wmf.wikidata_item_page_link as ipl
                ON wt.page_id = ipl.page_id
                AND wt.wiki_db = ipl.wiki_db
            WHERE wt.snapshot = '{wikitext_snapshot}'
            AND ipl.snapshot = '{item_page_link_snapshot}'
            AND wt.page_namespace = 0
            AND ipl.page_namespace = 0
            AND wt.page_redirect_title = ''
        """
    )

    if wiki_dbs:
        articles_df = articles_df.filter(F.col('wiki_db').isin(wiki_dbs))

    images_df = articles_df.select(
        'item_id',
        'page_id',
        'page_title',
        'wiki_db',
        extract_section_images('revision_text').alias('article_images'),
    )

    return images_df


def main() -> None:
    parser = argparse.ArgumentParser(
        description='Gather images available in Wikipedia articles from wikitext',
    )

    # Required
    parser.add_argument(
        '--wikitext-snapshot',
        required=True,
        metavar='YYYY-MM',
        help=('"wmf.mediawiki_wikitext_current" Hive monthly snapshot'),
    )
    parser.add_argument(
        '--item-page-link-snapshot',
        required=True,
        metavar='YYYY-MM-DD',
        help=('"wmf.wikidata_item_page_link" Hive weekly snapshot'),
    )
    parser.add_argument(
        '--output',
        required=True,
        metavar='/hdfs_path/to/parquet',
        help='HDFS path to output parquet',
    )

    # Optional
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '--wp-codes-file',
        dest='wiki_dbs',
        metavar='/path/to/file.json',
        type=load_wp_codes,
        default=load_wp_codes(str(resources.path('imagerec.data', 'wikipedias.json'))),
        help=(
            'path to JSON file with a list of Wikipedia language codes to process. '
            'Default: all Wikipedias, see "data/wikipedia.json"'
        ),
    )
    group.add_argument(
        '--wp-codes',
        nargs='*',
        dest='wiki_dbs',
        metavar='wp-code',
        type=wp_code_to_wiki_db,
        help=(
            'space-separated Wikipedia language codes to process. '
            'Example: ar en zh-yue'
        ),
    )
    parser.add_argument(
        '--coalesce',
        metavar='N',
        default=COALESCE,
        help=(
            'Control the amount of files per output partition. '
            'A higher value implies more files but a faster and lighter execution. '
            f'Default: {COALESCE}'
        ),
    )
    args = parser.parse_args()

    # Uncomment for prod
    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_session
    # spark = create_session(
    # app_name='section-alignment-image-suggestions',
    # type='yarn-large',
    # ship_python_env=True,
    # extra_settings={
    # 'spark.sql.shuffle.partitions': 2048,
    # 'spark.reducer.maxReqsInFlight': 1,
    # 'spark.shuffle.io.retryWait': '60s',
    # 'spark.executor.memoryOverhead': '3G',
    # 'spark.executor.memory': '10G',
    # 'spark.stage.maxConsecutiveAttempts': 10,
    # },
    # )

    images_df = get_article_images(
        spark,
        args.wikitext_snapshot,
        args.item_page_link_snapshot,
        args.wiki_dbs,
    )
    images_df.coalesce(args.coalesce).write.parquet(args.output, mode='overwrite')

    spark.stop()


if __name__ == '__main__':
    main()
